//
//  AppDelegate.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 16.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//


import UIKit
import PXPixieSDK_New
import SVProgressHUD
import CoreBluetooth
import ReachabilitySwift
import INTULocationManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CBPeripheralManagerDelegate, PXPixiePointDelegate {

    var window: UIWindow?
    var alertVC: UIViewController?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid

    var bluetoothPeripheralManager: CBPeripheralManager? //to detect if bluetooth is enabled/disabled
    var reachability: Reachability?

    // MARK: - App Lifecycle
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        if launchOptions?[UIApplicationLaunchOptionsLocationKey] != nil {
            NSLog("[pixiekid] launched application from a location event")
            processBackgroundLocationNotification()
            return true
        }
        
        DataManager.sharedInstance.initialize()

        
        if DataManager.sharedInstance.pixieManager.pixieUser == nil {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewControllerWithIdentifier("login_nav")
            self.window?.rootViewController = loginVC
        }
    
        setup()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {

    }

    func applicationDidEnterBackground(application: UIApplication) {
 
    }

    func applicationWillEnterForeground(application: UIApplication) {

    }

    func applicationDidBecomeActive(application: UIApplication) {
        
        if INTULocationManager.locationServicesState() != .Available {
            INTULocationManager.sharedInstance().requestLocationWithDesiredAccuracy(.City, timeout: 30, delayUntilAuthorized: true) { (location: CLLocation!, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) in
                NSLog("current location %@", location)
            }
        }
        
        checkAlert()
    }

    func applicationWillTerminate(application: UIApplication) {
        NSLog("[pixiekid] applicationWillTerminate")

    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        checkAlert()
    }

    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        NSLog("application didReceiveLocalNotification %@", notification)
    }
    
    // MARK: - CBPeripheralManagerDelegate

    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        var statusMessage = ""
        
        switch peripheral.state {
        case CBPeripheralManagerState.PoweredOn:
            statusMessage = "Bluetooth Status: Turned On"
            
        case CBPeripheralManagerState.PoweredOff:
            statusMessage = "Bluetooth Status: Turned Off"
            
        case CBPeripheralManagerState.Resetting:
            statusMessage = "Bluetooth Status: Resetting"
            
        case CBPeripheralManagerState.Unauthorized:
            statusMessage = "Bluetooth Status: Not Authorized"
            
        case CBPeripheralManagerState.Unsupported:
            statusMessage = "Bluetooth Status: Not Supported"
            
        default:
            statusMessage = "Bluetooth Status: Unknown"
        }
        
        print(statusMessage)
        
        checkAlert()
    }
    
    // MARK: - Reachability
    
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
            checkAlert()
        }
    }
    
    // MARK: - Misc

    func setup() {
        //UI
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Black)
        
        self.window!.tintColor = UIColor.lightGrayColor()
        
        let tabBarTitleTextAttributes: NSDictionary = [NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 10)!]
        UITabBarItem.appearance().setTitleTextAttributes(tabBarTitleTextAttributes as? [String: AnyObject], forState: .Normal)
        
        UINavigationBar.appearance().tintColor = UIColor.lightGrayColor()
        
        let navigationBarTitleTextAttributes: NSDictionary = [NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 18)!, NSForegroundColorAttributeName: UIColor.darkTextColor()]
        UINavigationBar.appearance().titleTextAttributes = navigationBarTitleTextAttributes as? [String: AnyObject]
        
        // Bluetooth
        
        let options = [CBCentralManagerOptionShowPowerAlertKey:false]
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
        
        // Reachability
        
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(reachabilityChanged),
                                                         name: ReachabilityChangedNotification,
                                                         object: reachability)
        do {
            try reachability?.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
        
        // Notifications
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(pushNotificationSettings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func checkAlert() {
        if bluetoothPeripheralManager?.state == .PoweredOff {
            presentAlert()
        } else if !CLLocationManager.locationServicesEnabled() {
            presentAlert()
        } else if INTULocationManager.locationServicesState() != .Available {
            presentAlert()
        } else if !reachability!.isReachable() {
            presentAlert()
        } else if UIApplication.sharedApplication().backgroundRefreshStatus != .Available {
            presentAlert()
        } else if !UIApplication.sharedApplication().isRegisteredForRemoteNotifications() {
            presentAlert()
        } else {
            if alertVC != nil {
                alertVC?.dismissViewControllerAnimated(true, completion: nil)
                alertVC = nil
            }
        }
    }
    
    func presentAlert() {
        if alertVC != nil {
            return
        }
        
        guard let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) else {
            return
        }
        
        guard let vc: UIViewController = storyboard.instantiateViewControllerWithIdentifier("alertVC") else {
            return
        }
        
        alertVC = vc
        self.window?.rootViewController?.presentViewController(vc, animated: false, completion: nil)
    }

    func beginBackgroundTask() {
        NSLog("[pixiekid] registering background task")
        backgroundTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
            [unowned self] in
            NSLog("[pixiekid] background task expiration handler called")

            self.showChildPointDisconnectedAlert()
            self.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        NSLog("[pixiekid] background task ended")
        UIApplication.sharedApplication().endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    func processBackgroundLocationNotification() {
        beginBackgroundTask()
        
        // If you start monitoring significant location changes and your app is subsequently terminated, the system automatically relaunches the app into the background if a new event arrives.
        // Upon relaunch, you must still subscribe to significant location changes to continue receiving location events.
        INTULocationManager.sharedInstance().subscribeToSignificantLocationChangesWithBlock({ (location: CLLocation!, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) in
            NSLog("[pixiekid] Significant Location Change <%@>", location)
        })
        
        let dataManager = DataManager.sharedInstance
        dataManager.initialize()
        let points = dataManager.pixieManager.getPairedPixiePoints({ (error) in
            NSLog("[pixiekid] error getting paried points")
            self.showChildPointDisconnectedAlert()
        })
        
        
        for point in points {
            if point == dataManager.childPoint {
                point.delegate = self
                dataManager.pixieManager.connectToPoint(point, errorBlock: { (error) in
                    NSLog("[pixiekid] error connecting to point")
                    self.showChildPointDisconnectedAlert()
                })
                return
            }
        }
    }
    
    func showChildPointDisconnectedAlert() {
        NSLog("[pixiekid] showing ChildPointDisconnectedAlert")

        let localNotification: UILocalNotification = UILocalNotification()
        localNotification.alertAction = NSLocalizedString("Open App", comment: "")
        localNotification.alertBody = NSLocalizedString("Could not detect a child nearby!", comment: "")
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.fireDate = NSDate()
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        endBackgroundTask()
    }

    //MARK: - PXPixiePointDelegate
    
    func pixiePointDidConnect(pixiePoint: PXPixiePoint) {
        NSLog("Child point found - did connect successfully to point: \(pixiePoint.macAddressString)")
    }
    
    func pixiePointDidDisconnect(pixiePoint: PXPixiePoint) {}
    
    func point(point: PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXReadBatteryResult) {}
    
    func point(point: PXPixiePoint, didUpdateBatteryStatus batteryStatus: PixieBatteryStatus) {}
    
    func didFinishPairing(point: PXPixiePoint, withResult result: PXPairingResult, failReason: String?) {}
    
    func didFinishUnpairing(point: PXPixiePoint, withResult result: PXPairingResult, failReason: String?) {}

}

