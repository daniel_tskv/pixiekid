//
//  PointTVC.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 17.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import PXPixieSDK_New

class PointCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var coordinatorImageView: UIImageView!
    @IBOutlet weak var childImageView: UIImageView!

    var point: PXPixiePoint? {
        didSet {
            setupPoint()
        }
        
    }
    
    var rangable = false {
        didSet {
            setupPoint()
        }
    }
    
    private func setupPoint() {
        guard let point = point else {
            resetView()
            return
        }
        
        if point.pointName != nil && point.pointName?.characters.count > 0 {
            nameLabel.text = point.pointName
        } else {
            nameLabel.text = "\(point.macAddressString)"
        }
        
        iconImageView.tintColor = point.pointColor.getColor()
        
        if point.pointState == PXState.CONNECTED {
            iconImageView.image = UIImage(named: "pin")?.imageWithRenderingMode(.AlwaysTemplate)
        } else {
            iconImageView.image = UIImage(named: "pin_outline")?.imageWithRenderingMode(.AlwaysTemplate)
        }
        
        let coordinatorPoint = DataManager.sharedInstance.coordinatorPoint
        if point == coordinatorPoint {
            coordinatorImageView.hidden = false
        } else {
            coordinatorImageView.hidden = true
        }
        
        let childPoint = DataManager.sharedInstance.childPoint
        if point == childPoint {
            childImageView.hidden = false
        } else {
            childImageView.hidden = true
        }
    }
    
    func resetView() {
        nameLabel.text = ""
        iconImageView.image = UIImage(named: "pin")
    }
    
    override func prepareForReuse() {
        resetView()
    }

}
