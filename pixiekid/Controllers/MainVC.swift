//
//  MainVC.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 16.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import INTULocationManager
import SVProgressHUD

class MainVC: UIViewController {

    var locationRequestID: INTULocationRequestID?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var toggleButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        toggleButton.layer.masksToBounds = true
        toggleButton.layer.cornerRadius = 4;
        
        setupView()
    }

    func setupView() {
        if DataManager.sharedInstance.protectionEnabled {
            imageView.tintColor = UIColor(red: 148/255.0, green: 207/255.0, blue: 76/255.0, alpha: 1)
            imageView.image = UIImage(named: "verified")?.imageWithRenderingMode(.AlwaysTemplate)
            textLabel.text = NSLocalizedString("Protected", comment: "")
            toggleButton.setTitle(NSLocalizedString("Disable Protection", comment: ""), forState: .Normal)
            
        } else {
            imageView.tintColor = UIColor(red: 244/255.0, green: 165/255.0, blue: 40/255.0, alpha: 1)
            imageView.image = UIImage(named: "alert")?.imageWithRenderingMode(.AlwaysTemplate)
            textLabel.text = NSLocalizedString("Not Protected", comment: "")
            toggleButton.setTitle(NSLocalizedString("Enable Protection", comment: ""), forState: .Normal)
        }
    }
    
    @IBAction func toggleProtection() {
        let protected = DataManager.sharedInstance.protectionEnabled
        if protected {
            disableProtection()
        } else {
            if DataManager.sharedInstance.childPoint == nil {
                SVProgressHUD.showInfoWithStatus(NSLocalizedString("Please select the Pixie Point that is attached to the child from the Points tab", comment: ""))
                return
            }
            enableProtection()
        }
        
        DataManager.sharedInstance.protectionEnabled = !protected
        setupView()
    }
    
    func enableProtection() {
        INTULocationManager.sharedInstance().subscribeToSignificantLocationChangesWithBlock({ (location: CLLocation!, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) in
            
        })
    }
    
    func disableProtection() {
        let locationManager: CLLocationManager = CLLocationManager()
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
