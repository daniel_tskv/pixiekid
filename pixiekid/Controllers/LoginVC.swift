//
//  FirstViewController.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 16.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import PXPixieSDK_New
import SVProgressHUD

enum LoginMode: String {
    case Login
    case Signup
}

class LoginVC: UITableViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var toggleButton: UIButton!
    
    var loginMode: LoginMode = .Login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.layer.masksToBounds = true
        doneButton.layer.cornerRadius = 4;
    }
    
    @IBAction func toggleButtonPressed(sender: UIButton) {
        if loginMode == .Login {
            //title = NSLocalizedString("Sign up", comment: "")
            doneButton.setTitle(NSLocalizedString("Sign up", comment: ""), forState: .Normal)
            toggleButton.setTitle(NSLocalizedString("Have an account?", comment: ""), forState: .Normal)
            loginMode = .Signup
        } else {
            //title = NSLocalizedString("Login", comment: "")
            doneButton.setTitle(NSLocalizedString("Login", comment: ""), forState: .Normal)
            navigationItem.rightBarButtonItem?.title = NSLocalizedString("Sign up", comment: "")
            toggleButton.setTitle(NSLocalizedString("Don't Have an account?", comment: ""), forState: .Normal)
            
            loginMode = .Login
        }
    }
    
    @IBAction func loginPressed(sender: AnyObject) {
        
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        
        if self.loginMode == .Login {
            login()
        } else {
            signup()
        }
    }
    
    func login() {
        let email = self.emailTF.text!
        let password = self.passwordTF.text!
        var errorMessage = ""
        
        
        SVProgressHUD.showWithStatus(NSLocalizedString("Loading...", comment: ""))
        
        DataManager.sharedInstance.pixieManager.login(email, userPassword: password, completion: { [weak self] (result) in
            if (result == PXAccountResult.OK) {
                print("logged in with SUCCESS: \(result.simpleDescription())")
                //DbContext.saveSession(email)
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                    self?.showMainView()
                })
            } else {
                errorMessage = result.simpleDescription()
                print("Could not login for another reason: \(errorMessage)")
                
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.showErrorWithStatus(errorMessage)
                })
            }
            
        }) { (error) in
            
            //DbContext.clearSession()
            errorMessage = error.simpleDescription()
            print("logged in with FAILURE = \(errorMessage)")
            
            dispatch_async(dispatch_get_main_queue(), {
                SVProgressHUD.showErrorWithStatus(errorMessage)
            })
        }
    }
    
    func signup() {
        let email = self.emailTF.text!
        let password = self.passwordTF.text!
        var errorMessage = ""
        
        
        SVProgressHUD.showWithStatus(NSLocalizedString("Loading...", comment: ""))
        
        DataManager.sharedInstance.pixieManager.signup(email, userPassword: password, completion: { [weak self] (result) in
            if (result == PXAccountResult.OK) {
                print("logged in with SUCCESS: \(result.simpleDescription())")
                //DbContext.saveSession(email)
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                    self?.showMainView()
                })
            } else {
                errorMessage = result.simpleDescription()
                print("Could not login for another reason: \(errorMessage)")
                
                dispatch_async(dispatch_get_main_queue(), {
                    SVProgressHUD.showErrorWithStatus(errorMessage)
                })
            }
            
        }) { (error) in
            
            //DbContext.clearSession()
            errorMessage = error.simpleDescription()
            print("logged in with FAILURE = \(errorMessage)")
            
            dispatch_async(dispatch_get_main_queue(), {
                SVProgressHUD.showErrorWithStatus(errorMessage)
            })
        }
    }
    
    func showMainView() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let tabVC = self.storyboard?.instantiateViewControllerWithIdentifier("tabbar")
        appDelegate.window?.rootViewController = tabVC
        
        DataManager.sharedInstance.coordinatorPoint = DataManager.sharedInstance.pixieManager.getCoordinator({ (error) in
            print("pixieManager.getCoordinator error: \(error.simpleDescription())")
        })
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === emailTF {
            passwordTF.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}

