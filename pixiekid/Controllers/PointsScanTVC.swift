//
//  PointsScanTVC.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 18.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import PXPixieSDK_New
import SVProgressHUD

class PointsScanTVC: UITableViewController, PXPixieManagerDelegate,PXPixiePointDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var toggleButton: UIButton!

    var scannedPoints: [PXPixiePoint] = []
    var scanning: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toggleButton.layer.masksToBounds = true
        toggleButton.layer.cornerRadius = 4;
        
        activityIndicator.stopAnimating()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        startScan()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        stopScan()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scannedPoints.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("scannedPointCell", forIndexPath: indexPath) as! ScannedPointCell
        //cell.pairButton.addTarget(self, action: #selector(pairPointPressed), forControlEvents: .TouchUpInside)
        
        let point = self.scannedPoints[indexPath.row]
        point.delegate = self
        
        cell.point = point
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let point:PXPixiePoint = self.scannedPoints[indexPath.row] else {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            return
        }
        
        stopScan()
        
        SVProgressHUD.showWithStatus(NSLocalizedString("Pairing \(point.macAddressString)...", comment: ""))
        DataManager.sharedInstance.pixieManager.pairPoint(point, errorBlock: { (error) in
            print("Pairing - could not connect")
            SVProgressHUD.showErrorWithStatus(error.simpleDescription())
        })
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - Actions
    
    @IBAction func backPressed() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func toggleScan() {
        if (scanning) {
            self.stopScan()
        } else {
            self.startScan()
        }
    }
    
    func startScan() {
        scanning = true
        activityIndicator.startAnimating()

        dispatch_async(dispatch_get_main_queue(), {
            self.toggleButton.setTitle("Stop Scan", forState: UIControlState.Normal)
        })
        
        DataManager.sharedInstance.pixieManager.delegate = self
        DataManager.sharedInstance.pixieManager.startScanningForPoints(PXScanType.NewPoint) { [weak self] (error) in
            print("Pairing - failed to open scan: \(error.simpleDescription())")
            dispatch_async(dispatch_get_main_queue(), {
                self?.scanning = false
                self?.toggleButton.setTitle("Start Scan", forState: UIControlState.Normal)
                self?.activityIndicator.stopAnimating()
            })
        }
    }
    
    func stopScan() {
        scanning = false
        activityIndicator.stopAnimating()

        dispatch_async(dispatch_get_main_queue(), {
            self.toggleButton.setTitle("Start Scan", forState: UIControlState.Normal)
        })
        
        DataManager.sharedInstance.pixieManager.delegate = self
        DataManager.sharedInstance.pixieManager.stopScanningForPoints({ [weak self] (error) in
            print("Pairing - failed to close scan: \(error.simpleDescription())")
            dispatch_async(dispatch_get_main_queue(), {
                self?.scanning = true
                self?.toggleButton.setTitle("Stop Scan", forState: UIControlState.Normal)
                self?.activityIndicator.startAnimating()
            })
        })
    }
    
    //MARK: PXPixieManagerDelegate
    
    func pixieManager(manager: PXPixieManager, didChangeStateFrom previousState: PixieManagerState, To currentState: PixieManagerState) {
        
    }
    
    func didGetRangesFromPoints(ranges: [PXRange]) {
    
    }
    
    func didScanPoint(point : PXPixiePoint) {
        
        print("Pairing - scanned: \(point.macAddressString)")
        
        if (self.scannedPoints.indexOf(point) < 0) {
            dispatch_async(dispatch_get_main_queue(), {
                let samePointCount = self.scannedPoints.filter({ (p) -> Bool in
                    return p.macAddressString == point.macAddressString
                }).count
                if (samePointCount > 0) {
                    return
                }
                point.delegate = self
                self.scannedPoints.insert(point, atIndex: 0)
                let indexPath = NSIndexPath(forItem: 0, inSection: 0)
                self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            })
        }
    }
    
    func pointsAreRangeable(points: [PXPixiePoint]){}
    
    //MARK: PXPixiePointDelegate
    
    func pixiePointDidConnect(pixiePoint: PXPixiePoint){
        print("Pairing - did connect successfully to point: \(pixiePoint.macAddressString)")
    }
    
    func pixiePointDidDisconnect(pixiePoint: PXPixiePoint){
        print("Pairing - did disconnect successfully from point: \(pixiePoint.macAddressString)")
    }
    
    func point(point: PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXReadBatteryResult) {}
    
    func point(point: PXPixiePoint, didUpdateBatteryStatus batteryStatus: PixieBatteryStatus){}
    
    func didFinishPairing(point: PXPixiePoint, withResult result: PXPairingResult, failReason: String?){
        
        print("Pairing - Finished pairing to point: \(point.macAddressString) with: \(result == PXPairingResult.FAIL  ? "FAILURE, reason: \(failReason!)": "SUCCESS")")
        
        if (result == PXPairingResult.SUCCESS) {
            SVProgressHUD.showSuccessWithStatus("Paired \(point.macAddressString)")
        } else {
            SVProgressHUD.showSuccessWithStatus("Pairing failed for \(point.macAddressString)")
        }
    }
    
    func didFinishUnpairing(point: PXPixiePoint, withResult result: PXPairingResult, failReason: String?){
        print("Pairing - Finished unpairing from point: \(point.macAddressString) with: \(result == PXPairingResult.FAIL ? "FAILURE description \(failReason!)": "SUCCESS")")
        if (result == PXPairingResult.SUCCESS) {
            SVProgressHUD.showSuccessWithStatus("UnPaired \(point.macAddressString)")
        } else {
            SVProgressHUD.showSuccessWithStatus("Unpairing failed for \(point.macAddressString)")
        }
    }
    


}
