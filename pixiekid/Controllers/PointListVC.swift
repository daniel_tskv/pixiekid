//
//  PointListVC.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 16.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import PXPixieSDK_New
import SVProgressHUD

class PointListVC: UITableViewController, PXPixieManagerDelegate, PXPixiePointDelegate {

    // MARK: - Properties

    var pairedPoints: [PXPixiePoint] = []
    var connectionTimeoutTimer: NSTimer?
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        DataManager.sharedInstance.pixieManager.delegate = self
        
        self.loadPoints()
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pairedPoints.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Paired Points", comment: "")
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pointCell", forIndexPath: indexPath) as! PointCell

        let point = self.pairedPoints[indexPath.row]
        point.delegate = self
    
        cell.point = point
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return NSLocalizedString("Unpair", comment: "")
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            guard let point:PXPixiePoint = self.pairedPoints[indexPath.row] else {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
                return
            }
            self.unpairPoint(point, atIndexpath: indexPath)
        }
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let point:PXPixiePoint = self.pairedPoints[indexPath.row] else {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            return
        }
        
        let alert = UIAlertController(title: point.macAddressString,
            message: NSLocalizedString("Select action:", comment: ""),
            preferredStyle: .ActionSheet)

        let coordinatorPoint = DataManager.sharedInstance.coordinatorPoint
        let isChildPoint: Bool = (point == DataManager.sharedInstance.childPoint)
        
        if !isChildPoint && point != coordinatorPoint {
            let childAction = UIAlertAction(title: NSLocalizedString("Use as Child Point", comment: ""), style: .Default) { [weak self] action in
                DataManager.sharedInstance.childPoint = point
                self?.tableView.reloadData()
            }
            alert.addAction(childAction)
        } else if point == DataManager.sharedInstance.childPoint {
            let childAction = UIAlertAction(title: NSLocalizedString("Remove as Child Point", comment: ""), style: .Default) { [weak self] action in
                
                if DataManager.sharedInstance.protectionEnabled {
                    SVProgressHUD.showInfoWithStatus(NSLocalizedString("Please disable protection before removing child point", comment: ""))
                } else {
                    DataManager.sharedInstance.childPoint = nil
                    self?.tableView.reloadData()
                }
            }
            alert.addAction(childAction)
        }
        
        if point.pointState == PXState.CONNECTED {

            if !isChildPoint && point != coordinatorPoint {
                let coordinatorAction = UIAlertAction(title: NSLocalizedString("Use as Phone Point", comment: ""), style: .Default) { [weak self] action in
                    DataManager.sharedInstance.coordinatorPoint = point
                    self?.tableView.reloadData()
                }
                alert.addAction(coordinatorAction)
            }
            
            let disconnectAction = UIAlertAction(title: NSLocalizedString("Disconnect", comment: ""), style: .Default) { [weak self] action in
                self?.disconnectPoint(point, atIndexpath: indexPath)
            }
            alert.addAction(disconnectAction)
        } else {
            let connectAction = UIAlertAction(title: NSLocalizedString("Connect", comment: ""), style: .Default) { [weak self] action in
                self?.connectPoint(point, atIndexpath: indexPath)
            }
            alert.addAction(connectAction)
        }
        
        let unpairAction = UIAlertAction(title: NSLocalizedString("Unpair", comment: ""), style: .Destructive) { [weak self] action in
            self?.unpairPoint(point, atIndexpath: indexPath)
        }
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil)
        
        alert.addAction(unpairAction)
        alert.addAction(cancel)
        
        presentViewController(alert, animated: true, completion: nil)
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    // MARK: - Actions
    
    private func clearPoints() {
        self.pairedPoints = []
        self.tableView.reloadData()
    }
    
    func loadPoints() {
        
        self.pairedPoints = []
        
        let points = DataManager.sharedInstance.pixieManager.getPairedPixiePoints({ (error) in
            print("error loading points")
        })
        
        for point in points {
            self.pairedPoints.append(point)
            point.delegate = self
        }
        self.tableView.reloadData()
    }
    
    func connectPoint(point: PXPixieSDK_New.PXPixiePoint, atIndexpath indexPath: NSIndexPath) {
        SVProgressHUD.showWithStatus("Connecting to \(point.macAddressString)...")
        
        connectionTimeoutTimer = NSTimer.scheduledTimerWithTimeInterval(30, //recommended pixie time
                                                                        target: self,
                                                                        selector: #selector(timeout),
                                                                        userInfo: nil,
                                                                        repeats: false)
        DataManager.sharedInstance.pixieManager.connectToPoint(point, errorBlock: { (error) in
            print("error connecting: \(error.simpleDescription())")
            SVProgressHUD.showErrorWithStatus(error.simpleDescription())
        })
        
        //[self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)]
    }
    
    func disconnectPoint(point: PXPixieSDK_New.PXPixiePoint, atIndexpath indexPath: NSIndexPath) {
        DataManager.sharedInstance.pixieManager.disconnectFromPoint(point, errorBlock: { (error) in
            SVProgressHUD.showErrorWithStatus(error.simpleDescription())
        })
        
        //[self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)]
    }
    
    func unpairPoint(point: PXPixieSDK_New.PXPixiePoint, atIndexpath indexPath: NSIndexPath) {
        DataManager.sharedInstance.pixieManager.unpairPoint(point, errorBlock: { (error) in
            SVProgressHUD.showErrorWithStatus(error.simpleDescription())
        })
        
        //[self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)]
    }

    func selectNewCoordinator() {
        
        for point in self.pairedPoints {
            if point == DataManager.sharedInstance.coordinatorPoint {
                continue
            }
            if point.pointState == .CONNECTED {
                DataManager.sharedInstance.coordinatorPoint = point
                self.tableView.reloadData()
                return
            }
        }
        
        DataManager.sharedInstance.coordinatorPoint = nil
        self.tableView.reloadData()
    }
    
    func indexPathForPixiePoint(point: PXPixiePoint) -> NSIndexPath? {
        let index = pairedPoints.indexOf{$0 === point}
        if index != nil  {
            return NSIndexPath(forRow: index!, inSection: 0)
        }
        return nil
    }
    
    func invalidateTimer() {
        if connectionTimeoutTimer != nil {
            connectionTimeoutTimer?.invalidate()
            connectionTimeoutTimer = nil
        }
        
    }
    
    func timeout() {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.showErrorWithStatus("Timeout")
        }
    }
    
    //MARK: - PXPixieManagerDelegate
    
    func pixieManager(manager: PXPixieManager, didChangeStateFrom previousState: PixieManagerState, To currentState: PixieManagerState) {
        
    }
    
    func didScanPoint(point: PXPixiePoint) {
        
    }
    
    func pointsAreRangeable(points: [PXPixiePoint]) {
        
        
    }
    
    func didGetRangesFromPoints(ranges: [PXRange]) {
        
    }
    
    //MARK: - PXPixiePointDelegate
    
    func didFinishPairing(pixiePoint: PXPixiePoint, withResult result: PXPairingResult, failReason : String?) {
        if (result == PXPairingResult.SUCCESS) {
            dispatch_async(dispatch_get_main_queue()) {
                self.loadPoints()
            }
        } else {
            SVProgressHUD.showErrorWithStatus(failReason)
        }
    }
    
    func didFinishUnpairing(pixiePoint: PXPixiePoint, withResult result: PXPairingResult, failReason : String?) {
        if (result == PXPairingResult.SUCCESS) {
            invalidateTimer()
            dispatch_async(dispatch_get_main_queue()) {
                self.loadPoints()
            }
        } else {
            SVProgressHUD.showErrorWithStatus(failReason)
        }
    }
    
    func pixiePointDidConnect(pixiePoint: PXPixiePoint) {
        print("Did connect to point \(pixiePoint.macAddressString)")
        
        invalidateTimer()
        SVProgressHUD.showSuccessWithStatus("Connected to \(pixiePoint.macAddressString)")

        if (DataManager.sharedInstance.coordinatorPoint == nil) {
            DataManager.sharedInstance.coordinatorPoint = pixiePoint
        }
        
        if let indexPath = indexPathForPixiePoint(pixiePoint) {
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            }
        }
    }
    
    func pixiePointDidDisconnect(pixiePoint: PXPixiePoint) {
        print("Did disconnect from point \(pixiePoint.macAddressString)")

        invalidateTimer()
        SVProgressHUD.showInfoWithStatus("Disconnected from \(pixiePoint.macAddressString)")
        
        if (DataManager.sharedInstance.coordinatorPoint == pixiePoint) {
            selectNewCoordinator()
        }
        
        if let indexPath = indexPathForPixiePoint(pixiePoint) {
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            }
        }
    }
    
    func point(point: PXPixiePoint, didUpdateBatteryStatus batteryStatus: PixieBatteryStatus) {
        
    }
    
    func point(point: PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXReadBatteryResult) {
        
    }
}
