//
//  AccountVC.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 16.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import UIKit
import PXPixieSDK_New
import SVProgressHUD


class AccountVC: UITableViewController {

    @IBOutlet weak var emailTF: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var pixieSDKVersionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let v = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as? String {
            let b = NSBundle.mainBundle().infoDictionary![kCFBundleVersionKey as String] as! String
            appVersionLabel.text = "\(v) (\(b))"
        }
        
        pixieSDKVersionLabel.text = PXVersionManager.sharedInstance.getSDKVersion()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //tableView.reloadData() //if user logs in or out
        
        setupUser()
    }
    
    func setupUser() {
        guard let user = DataManager.sharedInstance.pixieManager.pixieUser else {
            return
        }
        
        emailTF.text = user.userEmail
    }
    
    @IBAction func logoutPressed() {
        let alert = UIAlertController(title: nil, //NSLocalizedString("Logout", comment: ""),
                                      message: NSLocalizedString("Are you sure you want to log out?", comment: ""),
                                      preferredStyle: .ActionSheet)
        
        let action = UIAlertAction(title: NSLocalizedString("Logout", comment: ""), style: .Destructive) { [weak self] action in
            self?.logout()
        }
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil)
        alert.addAction(cancel)
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func logout() {
        var errorMessage = ""
        
        DataManager.sharedInstance.pixieManager.logout({ [weak self] (result) in
            if (result == PXAccountResult.OK) {
                print("logged out with SUCCESS: \(result.simpleDescription())")
                //DbContext.saveSession(email)
                SVProgressHUD.dismiss()
                
                let loginVC = self?.storyboard?.instantiateViewControllerWithIdentifier("login_nav")
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window?.rootViewController = loginVC
                
            } else {
                errorMessage = result.simpleDescription()
                print("Could not logout for another reason: \(errorMessage)")
                
                SVProgressHUD.showErrorWithStatus(errorMessage)
                
            }
        }) { (error) in
            errorMessage = error.simpleDescription()
            print("logged in with FAILURE = \(errorMessage)")
            
            SVProgressHUD.showErrorWithStatus(errorMessage)
        }
    }
    
    // MARK: - Table view delegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                changeEmail()
            } else if indexPath.row == 1 {
                changePassword()
            } else if indexPath.row == 2 {
                logoutPressed()
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    func changeEmail() {
        let alertController = UIAlertController(title: "Change Email", message: "Enter a new email address:", preferredStyle: .Alert)
        
        let emailAction = UIAlertAction(title: "Change", style: .Default) { (_) in
            let emailTF = alertController.textFields![0] as UITextField
            
            DataManager.sharedInstance.pixieManager.changeEmail(emailTF.text!, completion: { [weak self] (result) in
                if result != .OK {
                    SVProgressHUD.showErrorWithStatus(result.simpleDescription())
                } else {
                    SVProgressHUD.showSuccessWithStatus("Email Changed")
                    self?.setupUser()
                }
                }, errorBlock: { (error) in
                    SVProgressHUD.showErrorWithStatus(error.simpleDescription())
            })
        }
        emailAction.enabled = false
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Email"
            textField.keyboardType = .EmailAddress
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                emailAction.enabled = textField.text != ""
            }
        }
        
        alertController.addAction(emailAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func changePassword() {
        let alertController = UIAlertController(title: "Change Password", message: "Enter a new password:", preferredStyle: .Alert)
        
        let passAction = UIAlertAction(title: "Change", style: .Default) { (_) in
            let passTF = alertController.textFields![0] as UITextField
            
            DataManager.sharedInstance.pixieManager.changeEmail(passTF.text!, completion: { [weak self] (result) in
                if result != .OK {
                    SVProgressHUD.showErrorWithStatus(result.simpleDescription())
                } else {
                    SVProgressHUD.showSuccessWithStatus("Password Changed")
                    self?.setupUser()
                }
                }, errorBlock: { (error) in
                    SVProgressHUD.showErrorWithStatus(error.simpleDescription())
            })
        }
        passAction.enabled = false
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Password"
            textField.keyboardType = .ASCIICapable
            textField.secureTextEntry = true

            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                passAction.enabled = textField.text != ""
            }
        }
        
        alertController.addAction(passAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
