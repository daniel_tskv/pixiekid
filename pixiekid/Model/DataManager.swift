//
//  DataManager.swift
//  pixiekid
//
//  Created by Daniel Tsirulnikov on 17.8.2016.
//  Copyright © 2016 Daniel Tsirulnikov. All rights reserved.
//

import Foundation
import PXPixieSDK_New

class DataManager {
    
    static let sharedInstance = DataManager()
    
    var protectionEnabled: Bool {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey("protection")
        }
        set(newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "protection")
        }
    }
    
    var childPoint: PXPixiePoint? {
        get {
            guard let udidString = NSUserDefaults.standardUserDefaults().stringForKey("child_pixie") else {
                return nil
            }
            
            guard let childPixieID = PixieID(UUIDString: udidString) else {
                return nil
            }
            
            let points = DataManager.sharedInstance.pixieManager.getPairedPixiePoints({ (error) in
                print("error loading points")
            })
            
            for point in points {
                if point.pixieID.isEqual(childPixieID) {
                    return point
                }
            }
            return nil
        }
        set(newValue) {
            if newValue == nil {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("child_pixie")
            } else {
                let point: PXPixiePoint = newValue!
                NSUserDefaults.standardUserDefaults().setObject(point.pixieID.UUIDString, forKey: "child_pixie")
            }
        }
    }
    
    var pixieManager: PXPixieManager!
    var coordinatorPoint : PXPixiePoint? {
        didSet(newValue) {
            guard let newCoordinator = newValue else {
                return
            }
            pixieManager.setCoordinatorPoint(newCoordinator, errorBlock: { (error) in
                print("error: \(error.simpleDescription())")
            })
        }
    }
    
    private init() {


    }
    
    func initialize() {
        pixieManager = PXPixieManager() { (error) in
            if let _error = error {
                print("FATAL Error: \(_error.simpleDescription())")
            } else {
                print("Pixie SDK was initialized successfully")
            }
        }
        
        coordinatorPoint = pixieManager.getCoordinator({ (error) in
            print("pixieManager.getCoordinator error: \(error.simpleDescription())")
        })
        
        print("coordinatorPoint: \(coordinatorPoint)")
    }
}