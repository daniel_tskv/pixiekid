# PixieKid #

An iOS application for monitoring child's presence nearby. Useful in cases where a child can be forgotten, such as the car.
The app works with [Pixie Points](https://www.getpixie.com/) and developed for the Pixie app competition.

### About ###

The app includes common Pixie Point actions includes user authentication, showing paired points, pairing, unpairing, connecting and disconnecting points, and switching phone point.

The app works by selecting a Pixie Point as the child point (the user will attach the point to the child or put it in his/hers backpack for example).
Then the app monitors the device's location in the background (using the significant-change location service) and upon receiving a location change notification, the app tries to connect to the child point. If no child point was located nearby, the user's device will receive a notification.

### Usage ###

Download or clone the repository, use Cocoapods to install dependencies and run the application on a real device.

### Screenshots ###

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/1.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/2.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/3.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/4.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/5.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/6.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/1054b3ae094f1d86143935519e24a38b4fb07c5d/Screenshots/7.png)

![screenshot](https://bytebucket.org/daniel_tskv/pixiekid/raw/ac631c2dc69001de8e3ea6fe4a115e206c032ab1/Screenshots/8.png)