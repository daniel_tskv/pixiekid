//
//  PXTagsPairDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXTagsPairSimpleDataAdapter.h"

@interface PXTagsPairDataAdapter : PXTagsPairSimpleDataAdapter

//@property double timestamp;

@property double azimuth;
@property double inclination;
@property double srcHeight;
@property double dstHeight;


@end
