//
//  PXLEUserActionRequest.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXLERequest.h"



typedef NS_ENUM(NSUInteger, PXLEUserActionType) {
    WALK_REQUEST,
    TURN_REQUEST,
    TURN_CANCEL_REQUEST
};

@interface PXLEUserActionRequest : PXLERequest

@property enum PXLEUserActionType userActionType;

@end
