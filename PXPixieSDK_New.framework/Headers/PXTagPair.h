//
//  PXTagPair.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

@interface PXTagPair : PXDataAdapter 
@property (nonatomic, assign) UInt64 firstID;
@property (nonatomic, assign) UInt64 secondID;
@end
