//
//  PXInstantanousDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXOrientationDataAdapter.h"
#import "PXInsDataAdapater.h"
#import "PXDataAdapter.h"

@interface PXInstantanousDataAdapter : PXDataAdapter

@property BOOL valid;
@property double mobility;
@property (strong,nonatomic) PXOrientationDataAdapter *orientation ;
@property (strong,nonatomic) PXInsDataAdapater *insData;
@property double compassAzimuth;
@property BOOL yaw2mag_event;
@property double heading_traj_frame;
@property double heading_frames_offset;
@property double mag2head_offset_traj_frame;
@property int mag2head_accuracy;
@property bool misAlignment;

@end
