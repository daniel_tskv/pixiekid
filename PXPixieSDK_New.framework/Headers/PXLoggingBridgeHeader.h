//
//  PXLoggingBridgeHeader.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/16/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#ifndef PXSWBleManagerTestApp_PXLoggingBridgeHeader_h
#define PXSWBleManagerTestApp_PXLoggingBridgeHeader_h

#import "PXLoggingBridge.h"
#import <Foundation/Foundation.h>
#import <string.h>

double log_timestamp;
bool is_charly;
// Algo params
int range;
int angle;
char* algoParams;
// ****
char general_log_buffer[1024];

void logToFile(char* filename,char* logString)
{
    if(logString != NULL)
        [PXLoggingBridge log:logString toFile:filename];
}


#endif
