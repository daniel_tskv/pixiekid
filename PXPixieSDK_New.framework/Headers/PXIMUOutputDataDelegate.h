//
//  PXIMUOutputDataDelegate.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/11/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXIMUDataOutputAdapter.h"

@protocol PXIMUOutputDataDelegate <NSObject>

@required
-(void)didReceiveIMUDataOutput:(PXIMUDataOutputAdapter *)output;

@end


