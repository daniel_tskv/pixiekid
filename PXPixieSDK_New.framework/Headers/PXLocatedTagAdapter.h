//
//  PXLocatedTagAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"

@interface PXLocatedTagAdapter : PXDataAdapter

@property double x,y,z,range,azimuth,inclination,compassAzimuth,rangeScore,angularScore,estRangeError,estAngleError,speed, offset;
@property UInt64 tagID;

@end
