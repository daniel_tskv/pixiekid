//
//  PXIMUAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/11/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXIMUInputData.h"
#import "PXIMUOutputDataDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

@class PXIMUAdapter;

@protocol PXIMUResultDelegate <NSObject>

@required

-(void)didReceiveIMUDataOutput:(PXIMUDataOutputAdapter *)output;

@end

@interface PXIMUAdapter : NSObject <PXIMUOutputDataDelegate>


@property (weak,nonatomic) id<PXIMUResultDelegate> delegate;

-(instancetype)initWithDelegate:(id<PXIMUResultDelegate>)delegate andWithSensorRate:(double)sensorsRate;

-(void)recieveSensorBundleWithAccelerometer:(CMAccelerometerData*)accelerometerData atSystemTime:(double)accTimeStamp andGyroscope:(CMGyroData*)gyroscopeData atSystemTime:(double)gyroTimeStamp                                  andMagnometer:(CMMagnetometerData*)magnometerData atSystemTime:(double)magTimeStamp andDeviceMotion:(CMDeviceMotion*)deviceMotionData atSystemTime:(double)devMtnTimeStamp andHeading:(CLHeading*)heading atSystemTime:(double)headingTimeStamp;

-(void)resetIMU;

-(void)didReceiveIMUDataOutput:(PXIMUDataOutputAdapter *)output;
-(NSInteger)getQueueCount;

-(double)getIMUVersion;
@end
