//
//  PXUserGuidance.h
//  PXPixieSDK
//
//  Created by Almog Revivo on 6/11/15.
//  Copyright (c) 2015 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"

typedef NS_ENUM(NSUInteger, PXUserGuidanceStates) {
    GUIDE_IDLE,  // No data yet
    GUIDE_START_WALKING, // Data arrived but standing still before edge finished
    GUIDE_ON_TRACK, // request is done and user is moving
    GUIDE_TURN,  // first segment completed , turn now
    GUIDE_U_TURN, // Turn back (tag is somwhere behind)
    GUIDE_COMPLETED,
    GUIDE_KEEP_WALKING, // user stopped and should keep walking
    GUIDE_LOW_SIGNAL,
    GUIDE_NO_SIGNAL
};

@interface PXUserGuidance : PXDataAdapter

@property double stateStartTs;
@property double desiredAzimuth;
@property double azimuthUncertainty;
@property PXUserGuidanceStates guidanceState;
@property BOOL arEnabled;

@end
