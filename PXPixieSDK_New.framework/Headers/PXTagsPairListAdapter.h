//
//  PXTagsPairListAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/18/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

@interface PXTagsPairListAdapter : PXDataAdapter

@property (strong,nonatomic) NSMutableArray* pairsList;

@end
