//
//  PXLoggingBridge.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/16/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>


@class PXLogger;

@interface PXLoggingBridge : NSObject

@property PXLogger *logger;

+(void)log:(char*)log toFile:(const char*)fileName;
+(void)log:(NSString *)log;
+(void)save;

@end
