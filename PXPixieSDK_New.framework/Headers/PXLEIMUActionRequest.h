//
//  PXLEIMUActionRequest.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXLERequest.h"

typedef NS_ENUM(NSUInteger, PXLEIMUActionType) {
    START_IMU,
    STOP_IMU,
    RESET_IMU,
};

@interface PXLEIMUActionRequest : PXLERequest


@property enum PXLEIMUActionType imuActionType;

@end
