//
//  PXPDRDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

enum PXStepType {
    PX_FIRST_ET = 0,
    PX_LAST_ET,
    PX_TURN_ET,
    PX_MISALIGNMENT_ET,
    PX_NOMINAL_ET,
};

@interface PXPDRDataAdapter : PXDataAdapter

@property double stepStartTimestamp;
@property double stepEndTimestamp;
@property double stepLength;
@property enum PXStepType stepType;
@property double stepAzimuth;
@property double stepInclination;
@property double score;


@end
