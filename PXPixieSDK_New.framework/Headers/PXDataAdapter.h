//
//  PXDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@interface PXDataAdapter : NSObject <NSCoding>

@property double timestamp;

-(instancetype)initWithTimestamp:(double)timestamp;

@end
