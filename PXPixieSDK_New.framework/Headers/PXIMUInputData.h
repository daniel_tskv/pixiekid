//
//  PXIMUData.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PXIMUInputData : NSObject

@property double timestamp;

-(instancetype)initWithTimestamp:(double)timestamp;

@end
