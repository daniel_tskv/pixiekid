//
//  PXMapDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"
#import "PXTrajectoryDataAdapter.h"
#import "PXInstantanousDataAdapter.h"
#import "PXPDRDataAdapter.h"


@interface PXMapDataAdapter : PXDataAdapter

// filled with PXLocatedTagAdapters
@property NSMutableArray *locatedTags;

// filled with PXTagsPairDataAdapter
@property NSMutableArray *pairs;

@property PXTrajectoryDataAdapter *trajectoryForMap;
@property PXInstantanousDataAdapter *instantaneousForMap;
@property PXPDRDataAdapter *pdrForMap;
@property bool is_ar_avl;

@end
