//
//  PXLEMeasurementRequest.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXLERequest.h"



typedef NS_ENUM(NSInteger, MEASUREMENT_REQ_TYPE)
{
    MEASUREMENT_REQ_TYPE_FULL_ST,
    MEASUREMENT_REQ_TYPE_PARTIAL
};

typedef NS_ENUM(NSInteger, MEASUREMENT_BURST_TYPE)
{
    MEASUREMENT_BURST_TYPE_START = -1,
    MEASUREMENT_BURST_TYPE_CONTINUE = -2,
    MEASUREMENT_BURST_TYPE_STOP = 0,
    MEASUREMENT_BURST_TYPE_NONE = 1
};

@interface PXLEMeasurementRequest : PXLERequest

@property enum MEASUREMENT_REQ_TYPE requestType;
@property (strong,nonatomic) NSMutableArray* rangingTable; // Filled with PXTagPair
@property double requestTimestamp;
@property double sendTimestamp;
@property MEASUREMENT_BURST_TYPE burstType;
@property int burstAmount;
@property double burstDelay;
@property BOOL syncImuInst;
@property BOOL syncImuTraj;
@property int maxRetries;
@property BOOL isLERequest;


@end
