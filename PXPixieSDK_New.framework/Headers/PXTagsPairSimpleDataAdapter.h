//
//  PXTagsPairSimpleDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 10/26/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXTagPair.h"
@interface PXTagsPairSimpleDataAdapter : PXTagPair

@property double r;
@property double score;

@end
