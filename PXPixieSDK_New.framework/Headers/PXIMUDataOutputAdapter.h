//
//  PXIMUDataOutputAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/11/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"
#import "PXTrajectoryDataAdapter.h"
#import "PXPDRDataAdapter.h"
#import "PXInstantanousDataAdapter.h"

@interface PXIMUDataOutputAdapter : PXDataAdapter

@property (strong,nonatomic) NSMutableArray* trajectoryData;
@property (strong,nonatomic) PXPDRDataAdapter* pdrData;
@property (strong,nonatomic) PXInstantanousDataAdapter* instantaneousData;
@property double totalTraj;
@property int totalSteps;
@end
