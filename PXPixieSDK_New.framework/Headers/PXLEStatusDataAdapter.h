//
//  PXLEStatusDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 10/1/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

typedef NS_ENUM(NSUInteger, ALIGNMENT_STATUS_VALIDITY) {
    ALIGNMENT_STATUS_VALIDITY_VALID = 0x01

};


typedef NS_ENUM(NSUInteger, LShapeState) {
    LShapeState_IDLE,
    LShapeState_STARTED,
    LShapeState_IN_FIRST_EDGE,
    LShapeState_IN_SECOND_EDGE,
    LShapeState_DONE
};

typedef NS_ENUM(NSUInteger, TURN_STATUS) {
    TURN_STATUS_IDLE,
    TURN_STATUS_USER_INITIATED,
    TURN_STATUS_STARTED,
    TURN_STATUS_DONE
};


@interface PXLEStatusDataAdapter : PXDataAdapter

@property BOOL lShapeValid;
@property BOOL turnValid;

@property LShapeState lShapeState;
@property TURN_STATUS turnStatus;
@property double lshapeProgress;
@property double turnProgress;
@property int remap_calc_out_of_lock;
@property int remap_calc_sanity;

@end
