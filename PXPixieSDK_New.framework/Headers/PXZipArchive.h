//
//  PXZipArchive.h
//  PXPixieSDK
//
//  Created by Almog Lavi on 31/12/2015.
//  Copyright © 2015 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZipArchive.h"

@interface PXZipArchive : NSObject <ZipArchiveDelegate>

@property (strong, nonatomic) ZipArchive* zipper;

-(instancetype) init;

-(void) createZipWithName:(NSString*)fileName andPassword:(NSString*)password;
-(void) addFileToZip:(NSString*)filePath andFileName:(NSString*)fileName;
-(void) closeZip;



@end
