//
//  PXTrajectoryDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 8/31/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"


@interface PXTrajectoryDataAdapter : PXDataAdapter


@property double x;
@property double y;
@property double z;

@property double azimuth;
@property double inclination;
@property double compassAzimuth;
@property bool misalign;

@end
