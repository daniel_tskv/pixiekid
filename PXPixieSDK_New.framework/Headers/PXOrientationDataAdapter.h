//
//  PXOrientationDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

enum PXOrientDirection {
    PX_UNKNOWN_DIR_ET = 0,
    PX_HORIZONTAL_ET,
    PX_VERTICAL_ET,
};

enum PXOrientPosition {
    PX_UNKNOWN_POS_ET = 0,
    PX_PORTRAIT_UP_ET,
    PX_PORTRAIT_DOWN_ET,
    PX_LANDSCAPE_LEFT_ET,
    PX_LANDSCAPE_RIGHT_ET,
};

@interface PXOrientationDataAdapter : PXDataAdapter

@property double yaw;
@property double pitch;
@property double roll;
@property enum PXOrientDirection direction;
@property enum PXOrientPosition position;

@end
