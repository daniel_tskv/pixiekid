//
//  PXRangingDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Pixie on 9/8/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

@interface PXRangingDataAdapter : PXDataAdapter

@property UInt64 src;   // will be PixieTag
@property UInt64 dest;  // will be PixieTag
@property double range;
@property double score;
@property (strong,nonatomic) NSMutableArray* ranging_diagnostic_data;
@property (strong,nonatomic) NSMutableArray* ranging_pulse_shape_data;
@property int ranging_rssi;
@property double ble_rssi;


@end
