//
//  PXLEResponseDelegate.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#ifndef PXSWBleManagerTestApp_PXLEResponseDelegate_h
#define PXSWBleManagerTestApp_PXLEResponseDelegate_h

#import "PXLEResponse.h"
#import "PXMapDataAdapter.h"
#import "PXLEStatusDataAdapter.h"
#import "PXTagsPairSimpleDataAdapter.h"
#import "PXUserGuidance.h"
#import "PXLEStatistics.h"

@protocol PXLEResponseDelegate <NSObject>

@required
-(void)didRecieveResponse:(PXLEResponse *)response;
-(void)didRecieveMap:(PXMapDataAdapter *)map;
-(void)didRecieveStatus:(PXLEStatusDataAdapter *)status;
-(void)didRecieveMeasurementToDesired:(PXTagsPairSimpleDataAdapter *)measurementToDesired;
-(void)didReceiveTagsInBox:(NSMutableArray *)tagList andErrorList:(NSMutableArray *)errorList;
-(void)didReceiveMeasurments:(NSMutableArray *)measuremnts;
-(void)didReceiveUserGuidance:(PXUserGuidance *)userGuidance;
-(void)didReceiveLEStatistics:(PXLEStatistics *)leStatistics;
-(void)findDidStop;
@end

#endif
