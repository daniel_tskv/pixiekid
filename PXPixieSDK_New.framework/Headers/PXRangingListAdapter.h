//
//  PXRangingListAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

@interface PXRangingListAdapter : PXDataAdapter

// filled with PXRangingDataAdapter
@property (strong,nonatomic) NSMutableArray* rangingList;
//@property double requestTimestamp;
//@property double sendTimestamp;
@property double receiveTimestamp;
@property int requestSN;
@property int burstMeasurementIndex;
@property BOOL isLastInBurst;

@property double t1, t2, t3, t4, rangingTimestamp;

@end
