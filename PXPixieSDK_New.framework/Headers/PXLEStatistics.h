//
//  PXLEStatisticDataAdapter.h
//  PXPixieSDK
//
//  Created by Almog Lavi on 8/31/15.
//  Copyright (c) 2015 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"

@interface PXLEStatistics : PXDataAdapter

@property double le_totalTraj;
@property double totalTrajTillMap;
@property int numTurns;
@property int numTurnsTillMap;
@property int remapCalcOutOfLock;
@property int remapCalcSanity;
@property double imu_totalTraj;
@property int imu_totalSteps;
@end
