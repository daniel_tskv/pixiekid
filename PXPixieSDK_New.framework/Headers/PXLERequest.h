//
//  PXLERequest.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/9/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PXLERequest : NSObject

@property int requestSN;


- (instancetype)initWithSerialNumber:(int)serialNumber;


@end
