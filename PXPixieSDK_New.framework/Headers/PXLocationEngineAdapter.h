//
//  PXLocationEngineAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 8/27/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXLEResponseDelegate.h"

#import "PXLoggingBridgeHeader.h"
#import "PXIMUDataOutputAdapter.h"

@class PXRangingListAdapter;
@class PXInstantanousDataAdapter;
@class PXPDRDataAdapter;
@class PXTagsPairListAdapter;
@class PXUserReplyDataAdapter;
@class PXTagsPairSimpleDataAdapter;

@protocol PXLEResultDelegate <NSObject>

@required

-(void)didReceiveResponse:(PXLEResponse *)response;
-(void)didReceiveMap:(PXMapDataAdapter *)map;
-(void)didRecieveMeasurementFromMyPointToDesired:(PXTagsPairSimpleDataAdapter *)measurementToDesired;
-(void)didReceiveMeasurements:(PXMapDataAdapter *)measurementsMap;
-(void)didRecieveStatus:(PXLEStatusDataAdapter *)status;
-(void)didReceiveTagsInBox:(NSMutableArray *)tagList andErrorList:(NSMutableArray *)errorList;
-(void)didReceiveMeasurementsOnly:(NSMutableArray *)measurements;
-(void)didReceiveUserGuidance:(PXUserGuidance *)userGuidance;
-(void)didReceiveLEStatistics:(PXLEStatistics *)leStatistics;
-(void)findDidStop;
@end


@interface PXLocationEngineAdapter : NSObject <PXLEResponseDelegate>

@property (weak,nonatomic) id<PXLEResultDelegate> delegate;

- (instancetype)initWithResultDelegate:(id<PXLEResultDelegate>)delegate;

// data input methods

-(void) startLocationEngine;

-(void) stopLocationEngine;

-(void)findWithDesiredTag:(UInt64)desiredTag andWithMyTag:(UInt64)myTag andWithPNCTag:(UInt64)pncTag andWithHistoryData:(PXTagsPairListAdapter *)historyData andWithMeasurableTags:(NSMutableArray *)measurableTags andCloseDistance:(double)closeDistance;

-(void)resetFind;

-(void)getMeasurmentOnly:(PXTagsPairListAdapter *)rangePairs andIsPeriod:(BOOL)isPeriod andPeriod:(NSInteger)period;

-(void)resetMeasureOnly;

-(void)checkDesired:(NSMutableArray*)desiredTag withAnchors:(NSMutableArray*)anchores withWidth:(double)width andWithHeight:(double)height andWithTimestamp:(double)timestamp;

-(void)getLocationOfTag:(UInt64)tagId withAnchors:(NSMutableArray *)anchors andEstLocationError:(double)estLocErr;

-(void)resetLocationOfTag;

-(void)inputIMUDataOutput:(PXIMUDataOutputAdapter*) output;

-(void)inputTrajectory:(NSMutableArray *)trajectoryArray;

-(void)inputRange:(PXRangingListAdapter*)ranging;

-(void)inputNetworkChangeWithTags:(NSMutableArray*)tags;

-(void)inputInstantaneousData:(PXInstantanousDataAdapter*)instData;

-(void)inputPDRData:(PXPDRDataAdapter*)pdrData;

-(void)inputUserReply:(PXUserReplyDataAdapter *)userReply;

-(NSInteger)getQueueCount;

-(double)getLEVersion;

@end
