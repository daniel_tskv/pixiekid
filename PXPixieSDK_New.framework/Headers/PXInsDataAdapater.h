//
//  PXInsDataAdapater.h
//  PXSWBleManagerTestApp
//
//  Created by Almog Revivo on 9/10/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXDataAdapter.h"

@interface PXInsDataAdapater : PXDataAdapter

@property double linearAccX;
@property double linearAccY;
@property double linearAccZ;
@property double velocityX;
@property double velocityY;
@property double velocityZ;
@property double azimuth;
@property double inclination;

@end
