//
//  PXUserReplyDataAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 10/5/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#import "PXDataAdapter.h"

typedef NS_ENUM(NSUInteger, USER_REPLY) {
    STOP,
    RETRY,
    REFRESH,
    START
};

@interface PXUserReplyDataAdapter : PXDataAdapter

@property USER_REPLY userReply;
@property int requestSN;

@end
